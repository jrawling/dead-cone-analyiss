#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/Filter.hh"
#include "fastjet/contrib/JetsWithoutJets.hh"
#include "TLorentzVector.h"
#include "TString.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2F.h"
#include "TTree.h"
#include "Pythia8/Event.h"
#include <vector>
#include <map>


class DeadConeAnalysis{
 protected:
    //List of particles in the event
    std::vector<fastjet::PseudoJet> m_particles;

    //the clustering algorithm
    fastjet::JetDefinition m_jetDefinition;

    //produced jets after clustering with m_clusterSequence
    std::vector<fastjet::PseudoJet> m_jets;
    fastjet::ClusterSequence m_clusterSequence;

    //name of this displayer
    TString m_name;
    TString m_settingsFile;
    float m_minPt,m_Rjet;

    //hists
    TH1F* m_cutFlow;

    //an ntuple will be generated storing
    TTree* m_ntuple;

    //neutrino 
    TLorentzVector m_nu_p4;
    bool           m_isElectron;

    //lepton 
    TLorentzVector m_l_p4;
    std::vector<TLorentzVector> m_l_constiuents_p4;
    //hadronic top 
    TLorentzVector m_htop_p4;
    std::vector<TLorentzVector> m_htop_constiuents_p4;

    //b-quark 
    TLorentzVector m_bjet_p4;
    std::vector<TLorentzVector> m_bjet_constiuents_p4;

    //subjet of bquark
    TLorentzVector m_b_sj_p4;
    std::vector<TLorentzVector> m_b_sj_constiuents_p4;
    TLorentzVector m_g_sj_p4;
    std::vector<TLorentzVector> m_g_sj_constiuents_p4;

    //truth level stuff
    TLorentzVector m_t_p4;
    TLorentzVector m_b_p4;
    double m_m_nubl;
    double m_m_nublg;
    int m_nleptons;
    void ReadSettings();
    void DisplayWelcomeMessage();
    void clear_variables();
  public:
    DeadConeAnalysis(TString name, TString m_settingsFile);
    void ProcessEvent(Pythia8::Event &event);
    void Save(TString outputPrefix);
};
