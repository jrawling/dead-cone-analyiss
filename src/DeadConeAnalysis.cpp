#include "DeadConeAnalysis.h"
#include <iostream>
#include "TEnv.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TError.h"
#include <vector>
#include "TMath.h"
// #include "mMDTJetCollection.hh"
#include "fastjet/contrib/ModifiedMassDropTagger.hh"
using namespace std;
// using namespace fastjet; //NB because root isn't in a fucking namespace you cannot us fastjet namespace and TLorentzvector - BS! 
using namespace fastjet::jwj;

DeadConeAnalysis::DeadConeAnalysis( TString name, TString settingsFilePath):
  m_name(name),
  m_settingsFile(settingsFilePath)
{
  gErrorIgnoreLevel = kInfo;

  DisplayWelcomeMessage();
  ReadSettings();
  m_cutFlow = new TH1F("Cut Flow",";; N_{Events}", 11,0,11);
  m_cutFlow->GetXaxis()->SetBinLabel(1, "All events");
  m_cutFlow->GetXaxis()->SetBinLabel(2, "1 lepton");
  m_cutFlow->GetXaxis()->SetBinLabel(3, "p_{T}^{l} > 50 GeV");
  m_cutFlow->GetXaxis()->SetBinLabel(4, "p_{T}^{#nu} > 50 GeV");
  m_cutFlow->GetXaxis()->SetBinLabel(5, "2 jets");
  m_cutFlow->GetXaxis()->SetBinLabel(6, "Top tag");
  m_cutFlow->GetXaxis()->SetBinLabel(7, "b-jet decomposition");
  m_cutFlow->GetXaxis()->SetBinLabel(8, "b-subjet truth match");
  m_cutFlow->GetXaxis()->SetBinLabel(9, "b-subjet cut");
  m_cutFlow->GetXaxis()->SetBinLabel(10, "g-subjet cut");
  m_cutFlow->GetXaxis()->SetBinLabel(11, "pTg/pTt ");
}
void DeadConeAnalysis::DisplayWelcomeMessage(){
  std::cout <<"===============================================================" << std::endl;
  std::cout <<"===============================================================" << std::endl;
  std::cout <<"======================Top Tagging Tool=========================" << std::endl;
  std::cout <<"===============================================================" << std::endl;
}
std::vector<TString> Vectorize(TString str, TString sep=" ") {
  std::vector<TString> result;
   TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
    if (os->GetString()[0]!='#')
      result.push_back(os->GetString());
    else
      break;
  return result;
}

void DeadConeAnalysis::ReadSettings(){
  TEnv* settings = new TEnv(m_settingsFile);
  if(!settings){
    std::cerr << "Failed to read settings file: " << m_settingsFile << std::endl;
    exit(0);
  }

  //Read the settings
  m_Rjet   = settings->GetValue("Rjet",1.0f);
  m_minPt  = settings->GetValue("minPt",300.0f);

  m_ntuple = new TTree("NTuple", "Jet properties");

  //hadronic top 

  //neutrino + lepton
  m_ntuple->Branch("m_nu_p4" , &m_nu_p4);
  m_ntuple->Branch("m_l_p4" , &m_l_p4);
  m_ntuple->Branch("m_isElectron" , &m_isElectron);
  m_ntuple->Branch("m_l_constiuents_p4" , &m_l_constiuents_p4);
  m_ntuple->Branch("m_htop_p4" , &m_htop_p4);
  m_ntuple->Branch("m_htop_constiuents_p4" , &m_htop_constiuents_p4);
  m_ntuple->Branch("m_bjet_p4" , &m_bjet_p4);
  m_ntuple->Branch("m_bjet_constiuents_p4" , &m_bjet_constiuents_p4);
  m_ntuple->Branch("m_b_sj_p4" , &m_b_sj_p4);
  m_ntuple->Branch("m_b_sj_constiuents_p4" , &m_b_sj_constiuents_p4);
  m_ntuple->Branch("m_g_sj_p4" , &m_g_sj_p4);
  m_ntuple->Branch("m_g_sj_constiuents_p4" , &m_g_sj_constiuents_p4);
  m_ntuple->Branch("m_nublg" , &m_m_nublg);
  m_ntuple->Branch("m_nubl" , &m_m_nubl);
  m_ntuple->Branch("nleptons" , &m_nleptons);
}

void DeadConeAnalysis::clear_variables(){
   m_nu_p4 = TLorentzVector();
   m_l_p4 = TLorentzVector();
   m_l_constiuents_p4.clear();
    //lepton 
   m_l_p4 = TLorentzVector();
   m_l_constiuents_p4.clear();
    //hadronic top 
   m_htop_p4 = TLorentzVector();
   m_htop_constiuents_p4.clear();

    //b-quark 
   m_bjet_p4 = TLorentzVector();
   m_bjet_constiuents_p4.clear();

    //subjet of bquark
   m_b_sj_p4= TLorentzVector();
   m_b_sj_constiuents_p4.clear();
   m_g_sj_p4= TLorentzVector();
   m_g_sj_constiuents_p4.clear();
  m_m_nubl = -1;
  m_m_nublg = -1;
  m_nleptons = 0;
}
void DeadConeAnalysis::ProcessEvent(Pythia8::Event &event){
  //find all the particles in an event
  m_cutFlow->Fill(0);

  vector<fastjet::PseudoJet> particles;
  vector<TLorentzVector> leptons, neutrinos;
  fastjet::PseudoJet b_quark;
  static int eventNo = 0;eventNo++;
  clear_variables();
  for(int j = 0; j < event.size();j++ ){
    //loop over the particles
    if(event[j].id() == -5&&event[j].statusHepMC() == 23 ){
      b_quark = fastjet::PseudoJet(event[j].px(), event[j].py(),  event[j].pz(), event[j].e()) ;
    }
    if(event[j].isFinal()){

      if(event[j].id() == 12 || event[j].id() == -12 || event[j].id() == 14 || event[j].id() == -14){
        // m_nu_p4.SetPxPyPzE( event[j].px(), event[j].py(),  event[j].pz(), event[j].e()) ;
        TLorentzVector particle_p4;
        particle_p4.SetPxPyPzE(event[j].px(), event[j].py(),  event[j].pz(), event[j].e()) ;
        neutrinos.push_back( particle_p4 );
      }else
      
      if(event[j].id() == 11 || event[j].id() == -11 || event[j].id() == 13 || event[j].id() == -13){
        m_nleptons++;
        if(event[j].id() == 13 || event[j].id() == -13)
          m_isElectron = false;
        else
          m_isElectron = true;
        TLorentzVector particle_p4;
        particle_p4.SetPxPyPzE(event[j].px(), event[j].py(),  event[j].pz(), event[j].e()) ;
        leptons.push_back( particle_p4 );
      }
    }
  }

  //lepton 
  // cout << "n nu: " << neut/rinos.size() << endl;
  if( m_nleptons < 1 || neutrinos.size() < 1) return;
  m_cutFlow->Fill(1);

  int max_pt_index = 0;
  for(int i  =0; i < leptons.size();i++){
    if(leptons [i].Pt() > leptons [max_pt_index].Pt()  )
      max_pt_index = i;
  }
  m_l_p4 = leptons[max_pt_index];

  max_pt_index = 0;
  if( neutrinos.size() < 1) return;
  for(int i  =0; i < neutrinos.size();i++){

    if(neutrinos [i].Pt() > neutrinos [max_pt_index].Pt()  )
      max_pt_index = i;
  }
  m_nu_p4 = neutrinos[max_pt_index];

  for(int j = 0; j < event.size();j++ ){
    //loop over the particles
    /*
      0 : an empty entry, with no meaningful information and therefore to be skipped unconditionally (should not occur in PYTHIA);
      1 : a final-state particle, i.e. a particle that is not decayed further by the generator (may also include unstable particles that are to be decayed later, as part of the detector simulation);
      2 : a decayed Standard Model hadron or tau or mu lepton, excepting virtual intermediate states thereof (i.e. the particle must undergo a normal decay, not e.g. a shower branching);
      3 : a documentation entry (not used in PYTHIA);
      4 : an incoming beam particle;
      11 - 200 : an intermediate (decayed/branched/...) particle that does not fulfill the criteria of status code 2, with a generator-dependent classification of its nature; in PYTHIA the absolute value of the normal status code is used.
    */
    if(event[j].isFinal()){
      fastjet::PseudoJet particle  = fastjet::PseudoJet(   event[j].px(), event[j].py(),  event[j].pz(), event[j].e()) ;
      
      TLorentzVector particle_p4;
      particle_p4.SetPxPyPzE(  event[j].px(), event[j].py(),  event[j].pz(), event[j].e()) ;
      if( event[j].id() == 22 && m_l_p4.DeltaR(particle_p4) < 0.1  ){
        m_l_constiuents_p4.push_back( particle_p4 );
        continue;
      }
      
      if(event[j].id() == 11 || event[j].id() == -11 || event[j].id() == 13 || event[j].id() == -13) continue;
      if(event[j].id() == 12 || event[j].id() == -12 || event[j].id() == 14 || event[j].id() == -14) continue;

      particles.push_back(particle);
   }
  }
  for(auto p4 : m_l_constiuents_p4)
    m_l_p4 += p4;

  if( m_l_p4.Pt() < 50) return;
  m_cutFlow->Fill(2);
  //neutrino
  if( m_nu_p4.Pt() < 50) return;
  m_cutFlow->Fill(3);

  // //find jets - anti-kt,
  fastjet::JetDefinition jetDef = fastjet::JetDefinition(fastjet::antikt_algorithm ,m_Rjet);
  fastjet::ClusterSequence* clust_seq = new fastjet::ClusterSequence(particles,jetDef);
  m_jets = fastjet::sorted_by_pt(clust_seq->inclusive_jets(m_minPt));

  if(m_jets.size() < 2)return;
  m_cutFlow->Fill(4);

  //hadronic top - very loose ttagging 
  fastjet::PseudoJet b_jet,piece1,piece2,t_jet;
  bool top_tagged = false;
  bool found_bjet = false;
  vector<fastjet::PseudoJet> top_jets, b_jets;

  for ( auto& jet : m_jets){

    TLorentzVector jet_p4;
    jet_p4.SetPxPyPzE( jet.px(), jet.py(),  jet.pz(), jet.e()) ;
    if( jet_p4.DeltaR(m_l_p4) < 1.0){
      b_jets.push_back(jet);
      b_jet = jet;
        found_bjet = true;

    }else

    if( jet.m() < 210 || jet.m() > 130){
      t_jet = jet; //.push_back(jet);
      top_tagged = true;

    }
  }
  // cout << "n_t_jets = " << top_jets.size() << "    n_b_jets = " << b_jets.size() << endl;
  // return;
  m_htop_p4.SetPxPyPzE( t_jet.px(), t_jet.py(),  t_jet.pz(), t_jet.e()) ;
  m_bjet_p4.SetPxPyPzE( b_jet.px(), b_jet.py(),  b_jet.pz(), b_jet.e()) ;

  if( !top_tagged )
    return;
  m_cutFlow->Fill(5);

  if( !found_bjet )
    return;

  //sub-bjets
  bool passed_bsubjet = true;
  fastjet::contrib::ModifiedMassDropTagger mMDT(0.05);
  b_jet =  mMDT(b_jet);
  if(b_jet == 0)
    return;

  //need to apply mMDT here toclean up the jet 

  if(!b_jet.has_parents(piece1, piece2)){
    std::cerr << "WARNING: Attempted to find substructure variables for jet collection: "
              << m_name << "but found jet has no parents" << std::endl;
    return;
  } 
  m_cutFlow->Fill(6);

  // //   //sort the subjets by their mass
  fastjet::PseudoJet b_sj, g_sj;
  if ( b_quark.delta_R(piece1) < 0.3){
    b_sj = piece1;
    g_sj = piece2;
  }else{
    g_sj = piece1;
    if ( b_quark.delta_R(piece2) < 0.3)
      b_sj = piece2;
    else{
      return;
    }
  }
  m_cutFlow->Fill(7);
  m_b_sj_p4.SetPxPyPzE( b_sj.px(), b_sj.py(),  b_sj.pz(), b_sj.e()) ;
  m_g_sj_p4.SetPxPyPzE( g_sj.px(), g_sj.py(),  g_sj.pz(), g_sj.e()) ;


  if(b_sj.pt() < 50)return;
  m_cutFlow->Fill(8);


  if(g_sj.pt() < 25)return;
  m_cutFlow->Fill(9);



  TLorentzVector BLT, BLTg;
  BLT += m_nu_p4;
  BLT += m_b_sj_p4;
  BLT += m_l_p4;

  BLTg += m_nu_p4;
  BLTg += m_b_sj_p4;
  BLTg += m_l_p4;
  BLTg += m_g_sj_p4;

  if(g_sj.pt()/BLT.Pt() < .05)return;
  m_cutFlow->Fill(10);

  m_m_nubl = BLT.M();
  m_m_nublg = BLTg.M();

  m_ntuple->Fill();

}

void DeadConeAnalysis::Save(TString outputPrefix){
  TFile* outFile = new TFile(outputPrefix+"/"+m_name+".root", "RECREATE");

  // for( const auto&  jetCollect : m_jetCollections)
  //   jetCollect->Save(outFile);
  m_ntuple->Write();
  m_cutFlow->Write();
  outFile->Close();
  delete outFile;
}
