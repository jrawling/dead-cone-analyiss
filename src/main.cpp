/*
 * This tool should produce a d23 and tau_32
 *
 */
#include "Pythia8/Pythia.h"
using namespace Pythia8;

#include "TString.h"
#include "TEnv.h"
#include "TFile.h"
#include "TH1.h"

#include "DeadConeAnalysis.h"
int main(int argc, char* argv[]) {
  //Open the settings file
  TString outFilePath ( argc < 2 ? "share/settings.config" : argv[1]);
  TEnv* settings = new TEnv(outFilePath);
  if(!settings){
    std::cerr << "Failed to read settings file: " << argv[1] << std::endl;
    return -1;
  }

  //Read the settings
  TString inputFilePath   = settings->GetValue("InputFile","share/unweighted_events.lhe");
  TString outputFilePath  = settings->GetValue("OutputFile","out/");
  TString name            = settings->GetValue("Name","MCSample_UnknownType");
  int maxEvents           = settings->GetValue("MaxEvents",100);
  bool generateQCDJets    = settings->GetValue("generateQCDJets",false);

  //The top tagging tool
  DeadConeAnalysis* topTaggerTool = new DeadConeAnalysis(name, 1.0  );

  if(generateQCDJets)
    std::cout << "Generating pythia8 multijet events" << std::endl;
  else
    std::cout <<  "In file: " <<  inputFilePath << std::endl;

  std::cout <<"Output folder: " <<  outputFilePath << std::endl;
  std::cout <<"===============================================================" << std::endl;

  // Initialize Les Houches Event File run. List initialization information.
  Pythia pythia;
  // pythia.readString("Next:numberShowInfo = 1");
  // pythia.readString("Next:numberShowProcess = 1");
  // pythia.readString("Next:numberShowEvent = 0");
  pythia.readString("Beams:eCM = 13000.");
  pythia.readString("Beams:idA = 2212");
  pythia.readString("Beams:idB = 2212");

  //
  // pythia.readString("HadronLevel:all = off");
  if(generateQCDJets){
    pythia.readString("PDF:lepton = off");
    // Process selection.
    pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
    // Switch off all Z0 decays and then switch back on those to quarks.
    pythia.readString("23:onMode = off");
    pythia.readString("23:onIfAny = 1 2 3 4 5");
    pythia.readString("PhaseSpace:pTHatMin = 3000.");
  }else{
    // pythia.readString("PDF:lepton = off");
    pythia.readString("Top:ffbar2ttbar(s:gmZ) = on");
    pythia.readString("PhaseSpace:pTHatMin = 300.");

    pythia.readString("PartonLevel:ISR = on");
    pythia.readString("PartonLevel:FSR = on");
    pythia.readString("PartonLevel:MPI = on");
        //!specify W+- decays only to quarks
    pythia.readString("24:onMode = off");
    pythia.readString("24:onIfAny = 1 -2 3 4 -5 11 12 13 14");
  }

  // pythia.readString("Next:numberShowEvent = 0");
  // pythia.readString("Init:showProcesses = 1");
  pythia.init();
  Pythia8::Event& event = pythia.event;


  // Begin event loop; generate until none left in input file.
  for (int iEvent = 0; iEvent < maxEvents ; ++iEvent) {
    // Generate events, and check whether generation failed.
    if (!pythia.next()) {
      // If failure because reached end of file then exit event loop.
      if (pythia.info.atEndOfFile())
        break;
      break;
    }
    topTaggerTool->ProcessEvent(event);

  // End of event loop.
  }
  
  topTaggerTool->Save(outputFilePath);
  delete topTaggerTool;

  std::cout <<"===============================================================" << std::endl;
  std::cout <<"Saved to: " <<  outputFilePath << std::endl;
  std::cout << "Done." << std::endl;
  std::cout <<"===============================================================" << std::endl;
  // Done.
  return 0;
}
