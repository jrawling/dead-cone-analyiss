#################################################################################
# Author: Jacob Rawling (jrawling@cern.ch)                                      #
# Date: Nov 2016                                                            	  #
#																																								#
# Makefile for a top tagging ROC curve plott																		#
#																																								#
#################################################################################
OUTPUTNAME=bin/EfficiencyEvaluator.exe
SOURCES=src/*.cpp
INCLUDES=include
CC=g++

#PYTHIA CONFIGURATION
PYTHIAPATH=/Users/jrawling/pythia8223/
PREFIX_BIN=$(PYTHIAPATH)bin
PREFIX_INCLUDE=$(PYTHIAPATH)include
PREFIX_LIB=$(PYTHIAPATH)lib
PREFIX_SHARE=$(PYTHIAPATH)/share/Pythia8

#ROOT CONFIGURATION
ROOTSYS=/usr/local/root-6.06.08
ROOT_BIN=$(ROOTSYS)/bin/
ROOT_INCLUDE=$(ROOTSYS)/include/
ROOT_LIB=$(ROOTSYS)/lib/

#FASTJET CONFIGURATION
FASTJETPATH=/Users/jrawling/fastjet-install/

#libs
PYTHIA_LIB=/Users/jrawling/pythia8223/lib/libpythia8.a \
		   $(FASTJETPATH)lib/libNsubjettiness.a \
		   $(FASTJETPATH)lib/libJetsWithoutJets.a \
		   $(FASTJETPATH)lib/libRecursiveTools.a

DICT=bin/test_dict.c

#COMPILER SETTINGS
CXX_COMMON=-O2 -ansi -pedantic -W -Wall -Wno-shadow -fPIC -std=c++0x -fdiagnostics-color -g 
CXX_COMMON:=-I$(PREFIX_INCLUDE) -I$(ROOT_INCLUDE) -I$(INCLUDES)  $(CXX_COMMON) -Wl,-rpath $(PREFIX_LIB) -ldl -Wl,-rpath $(ROOT_LIB) `$(ROOT_BIN)root-config --glibs` 

dict:
	rootcint $(DICT) include/linkdef.h 
all:
	@$(CC) $(SOURCES) $(DICT) $(PYTHIA_LIB)  `$(FASTJETPATH)bin/fastjet-config  --libs --plugins --cxxflags` $(CXX_COMMON) -o $(OUTPUTNAME) 


$(PYTHIA_LIB):
	$(error Error: PYTHIA must be built, please run "make"\
                in the top PYTHIA directory)

clean:
	rm $(OUTPUTNAME)
